import dotenv
import os

dotenv.load_dotenv()

DISCORD_KEY = os.getenv('DISCORD_KEY')

# ----- BOT SETTINGS -----
BOT_NAME = "Evergreen"
BOT_PREFIX = ";"
BOT_BAD_COMMAND_RESPONSE = False
BOT_BAD_COMMAND_RESPONSE_MESSAGE = "Bad command or not understood."