from discord import Client
import natluk_bot.settings as settings

# quick and dirty, but good enough for now
COMMAND_MAPPING = {
    # as for now, all commands should have their main keyword stored here as key
    # example:
    # 'help': HelpCommand
}

MIDDLEWARES = [
    # middlewares should go here as classes
]


class NatlukClient(Client):

    async def on_ready(self):
        print(f"BOT {settings.BOT_NAME} started!")

    async def on_message(self, message):
        if message.author != self.user:
            await self.run_middlewares(message)
            if message.content.startswith(settings.BOT_PREFIX):
                if (command_result := await self.run_command(message)) is not None:
                    await message.channel.send(command_result)

    async def run_middlewares(self, message):
        for middleware in MIDDLEWARES:
            middleware.run(message)

    async def run_command(self, message):
        content = message.content[1:]
        command_keyword, *arg_list = content.split(" ")
        if command := COMMAND_MAPPING.get(command_keyword):
            return command.run(arg_list)
        elif settings.BOT_BAD_COMMAND_RESPONSE:
            return settings.BOT_BAD_COMMAND_RESPONSE_MESSAGE
