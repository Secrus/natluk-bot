

class BaseCommand:

    @staticmethod
    def run(*args):
        raise NotImplementedError(
            "This method is not implemented yet"
        )
