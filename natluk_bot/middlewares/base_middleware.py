

class BaseMiddleware:

    @staticmethod
    def run(*args):
        raise NotImplementedError(
            "This method was not implemented yet"
        )