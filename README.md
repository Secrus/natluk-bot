# Natluk Discord Bot

This is README of Natluk Discord Bot. If you are reading this, it means you want to participate in developing it. As for
now, bot is not even barebones. There is just basic structure implemented and its crude and dirty right now.
Anyway, how to use whats already here:

## Few rules before we begin

* All development should be done in English.
* Make commits meaning full. `"alkdja"` and `"Changes"` are not meaningfull names. Don't write poems, but please, just 
  specify what you were working on, fe `"Added changes to argument validation in ExampleCommand"`. 
* Code without tests should not be considered good code. In specific cases, code might lack tests, but there should be 
  intent to fix that in sure or mark it as additional task to be done.
* All changes to code must be done with `__version__` update (variable in `natluk_bot/__init__.py`) according to SemVer
  standards   
* Code should be formatted with `black` and be compliant with PEP-8 standards. (NOTE: Tooling automation is TBD)

## Commands

As "command" we mean every message that is not sent by bot and starts with right prefix (see `settings` module) and
defines some action. If action returns output, it should be either Python string or dicord embed from `disord.py` package
New commands should be defined as subclasses of class`BaseCommand` in `commands.base_command` and 
implement `run()` method, which takes list of arguments used with "keyword"
which is command name. `run()` method must return either string (Python `str` type) or discord embed.

## Middleware

As "middleware" we mean every action that takes place for every sent message. Example of that can be message per user
counting or tracking of user activity etc. New middlewares should be defined as subclasses of class `BaseMiddleware` in
`middlewares` module and implement `run()` method, which takes message object as argument and returns nothing. 

## Testing
Because testing discord commands is difficult and kinda makes zero sense, architecture proposed here makes it possible to
test Commands. Each Command added to bot, should have tests attached in `tests` module. Tests should cover as much corener 
cases and command uses as possible, without  going into extremes (consult Original Author if not sure). Commands without 
tests, will not be added to codebase. 

### Software side of testing

Tests are using `pytest` as testing framework. All tests should be functions starting with `test_` prefix and placed in
files named as `test_<feature>`. 


## Ending words

This project is still new and evolving. Don't consider anything here as set in stone and please, come forward with ideas 
for features and improvements.